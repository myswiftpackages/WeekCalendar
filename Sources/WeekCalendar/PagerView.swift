//
//  SwiftUIView.swift
//  
//
//  Created by vinodh kumar on 01/08/22.
//

import SwiftUI

@available(iOS 14.0, *)
public struct PagerView<Content: View>: View {
    
    let pageCount: Int
    @Binding var currentIndex: Int
    public let content: Content
    @GestureState private var translation: CGFloat = 0
    
    public init(pageCount: Int, currentIndex: Binding<Int>, @ViewBuilder content: () -> Content) {
        self.pageCount = pageCount
        self._currentIndex = currentIndex
        self.content = content()
    }
    
    public var body: some View {
        GeometryReader { geometry in
            HStack(spacing: 0) {
                self.content.frame(width: geometry.size.width)
            }
            .frame(width: geometry.size.width, alignment: .leading)
            .offset(x: -CGFloat(self.currentIndex) * geometry.size.width)
            .offset(x: self.translation)
            .gesture(
                DragGesture().updating(self.$translation) { value, state, _ in
                    state = value.translation.width
                }.onEnded { value in
                    let offset = value.translation.width / geometry.size.width
                    if offset < 0 {
                        self.currentIndex = min(self.pageCount - 1, self.currentIndex + 1)
                    } else {
                        self.currentIndex = max(0, self.currentIndex - 1)
                    }
                }
            )
        }
    }
}
