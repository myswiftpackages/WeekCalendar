import SwiftUI

@available(iOS 14.0, *)
public struct WeekCalendar<Content: View>: View {
    
    @Environment(\.calendar) var calendar
    @Binding var selectedDate: Date
    @Binding var selectToday: Int
    @Binding var updateView: Int
    @State var currentPage = 1
    @State var days: [Date] =  Self.getWeeks(for: Date())
    public let content: (Date) -> Content
    
    public init(selectedDate: Binding<Date>, selectToday: Binding<Int>, updateView: Binding<Int>, content: @escaping (Date) -> Content) {
        self._selectedDate = selectedDate
        self.content = content
        self._selectToday = selectToday
        self._updateView = updateView
    }
    
    
    public var body: some View {
        PagerView(pageCount: days.count, currentIndex: $currentPage) {
            ForEach(days, id: \.self) { day in
                WeekView(date: day) { date in
                    self.content(date)
                        .onTapGesture {
                            selectedDate = date
                        }
                }
                .padding(4)
            }
        }
        .frame(maxWidth: .infinity)
        .onChange(of: currentPage) { newValue in
            if newValue == 0 {
                self.days = Self.getWeeks(for: days[0])
                self.currentPage = 1
            } else if newValue == 2 {
                self.days = Self.getWeeks(for: days[2])
                self.currentPage = 1
            }
        }
        .onChange(of: selectToday) { _ in
            withAnimation(.linear(duration: 0.15)) {
                self.days = Self.getWeeks(for: Date())
                self.selectedDate = Date()
            }
        }
        .onChange(of: updateView) { _ in
            withAnimation(.linear(duration: 0.15)) {
                self.days = Self.getWeeks(for: selectedDate)
            }
        }
        .onAppear {
            self.days = Self.getWeeks(for: selectedDate)
        }
    }
    
    static public func getWeeks(for date: Date) -> [Date] {
        let range = -1...1
        let weeks = range.map { idx in
            return Calendar(identifier: .gregorian).date(byAdding: .weekOfMonth, value: idx, to: date)!
        }
        return weeks
    }
    
}
