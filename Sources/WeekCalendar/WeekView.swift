//
//  SwiftUIView.swift
//  
//
//  Created by vinodh kumar on 01/08/22.
//

import SwiftUI

@available(iOS 14.0, *)
public struct WeekView<DateView: View>: View{
    
    @Environment(\.calendar) var calendar
    public let date: Date
    public let content: (Date) -> DateView

    var days: [Date] {
        let daysMonth = (-3 ..< 4).compactMap { calendar.date(byAdding: .day, value: $0, to: date) }
        return daysMonth
    }
    
    public init(
        date: Date,
        @ViewBuilder content: @escaping (Date) -> DateView
    ) {
        self.date = date
        self.content = content
    }

    public var body: some View {
        HStack {
            ForEach(days, id: \.self) { date in
                HStack {
                    self.content(date)
                }
            }
        }
    }
    
}
