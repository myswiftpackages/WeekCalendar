# WeekCalendar

A description of this package.

A Weekly Calendar similar to apple calendar and swipe to load previous/next week.

## How to use

pass you view for calendar numbers in the view modifier Week Calendar

e.g.
WeekCalendar(date: selectedDate, selectedDate: $selectedDate) { date in
    VStack {
        HStack {
            Spacer()
            Text("4")
                .font(.caption)
                .padding(4)
                .overlay(
                    Circle()
                        .stroke(lineWidth: 1)
                )
                .padding(.trailing, 4)
        }
        Text(String(self.calendar.component(.day, from: date)))
            .font(.headline)
        Text(date.getDay()).font(.caption)
    }
    .padding(.vertical, 4)
    .frame(minWidth: 35)
    .background(date == selectedDate ? Color.blue : Color.green)
    .clipShape(RoundedRectangle(cornerRadius: 4))
    .frame(maxWidth: .infinity)
}
}
